import { DraftState } from "./State/DraftState"
import { States } from "./State/States"

export class State {
  private value:States = new DraftState()

  public publishBy(user:string):void {
    this.value = this.value.publishBy(user)
  }

  public reject():void {
    this.value = this.value.reject()
  }

  public toString():string {
    return this.value.toString()
  }
}
