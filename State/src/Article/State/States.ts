
export interface States {
  publishBy(user:string):States
  toString():string
  reject():States
}
