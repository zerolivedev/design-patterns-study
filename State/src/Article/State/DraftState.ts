import { ModerationState } from "./ModerationState"
import { PublishedState } from "./PublishedState"
import { States } from "./States"

export class DraftState implements States {
  private value:string = "draft"

  public publishBy(user:string):States {
    if (this.isAdmin(user)) {
      return new PublishedState()
    } else {
      return new ModerationState()
    }
  }

  public reject():States {
    return this
  }

  public toString():string {
    return this.value
  }

  private isAdmin(user:string):boolean {
    return (user === 'admin')
  }
}
