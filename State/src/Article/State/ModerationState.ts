import { PublishedState } from "./PublishedState"
import { DraftState } from "./DraftState"
import { States } from "./States"

export class ModerationState implements States {
  private value:string = "moderation"

  public publishBy(user:string):States {
    if (this.isAdmin(user)) {
      return new PublishedState()
    }

    return this
  }

  public reject():States {
    return new DraftState()
  }

  public toString():string {
    return this.value
  }

  private isAdmin(user:string):boolean {
    return (user === 'admin')
  }
}
