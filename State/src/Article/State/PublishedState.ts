import { DraftState } from "./DraftState"
import { States } from "./States"

export class PublishedState implements States {
  private value:string = "published"

  public publishBy(_user:string):States {
    return this
  }

  public reject():States {
    return new DraftState()
  }

  public toString():string {
    return this.value
  }
}
