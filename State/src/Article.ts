import { State } from "./Article/State"

export class Article {
  private currentState:State = new State()

  public publishBy(user:string):void {
    this.currentState.publishBy(user)
  }

  public reject():void {
    this.currentState.reject()
  }

  public state():string {
    return this.currentState.toString()
  }
}
