import { expect } from "chai"
import { Article } from "../src/Article"

describe("Article State", () => {
  const nonAdminUser:string = 'nonAdminUser'
  const adminUser:string = 'admin'

  it("is a draft on creation", () => {

    const article:Article = new Article()

    expect(article.state()).to.eq("draft")
  })

  it("pass to moderation when a draft is published by a non-admin", () => {
    const article:Article = new Article()

    article.publishBy(nonAdminUser)

    expect(article.state()).to.eq("moderation")
  })

  it("pass to publish when a draft is published by an admin", () => {
    const article:Article = new Article()

    article.publishBy(adminUser)

    expect(article.state()).to.eq("published")
  })

  it("pass to draft when the moderation reject it", () => {
    const article:Article = new Article()
    article.publishBy(nonAdminUser)

    article.reject()

    expect(article.state()).to.eq("draft")
  })

  it("pass to publish when an admin publish a moderated article", () => {
    const article:Article = new Article()
    article.publishBy(nonAdminUser)

    article.publishBy(adminUser)

    expect(article.state()).to.eq("published")
  })

  it("does not pass to publish when a non-admin publish a moderated article", () => {
    const article:Article = new Article()
    article.publishBy(nonAdminUser)

    article.publishBy(nonAdminUser)

    expect(article.state()).to.eq("moderation")
  })

  it("pass to draft when a published article is rejected", () => {
    const article:Article = new Article()
    article.publishBy(nonAdminUser)
    article.publishBy(adminUser)

    article.reject()

    expect(article.state()).to.eq("draft")
  })
})
