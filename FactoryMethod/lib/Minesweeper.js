
const BombPosition = class {
  check() {
    return 'BOOM!!!!'
  }
}

const EmptyPosition = class {
  check() {
    return ''
  }
}

const Board = class {
  static with(config) {
    return new Board(config)
  }

  constructor(config) {
    this.positions = []

    if (config.bombs === 0) {
      this.positions.push(new EmptyPosition())
    } else {
      this.positions.push(new BombPosition())
    }
  }

  checkPosition(id) {
    const index = id - 1

    return this.positions[index].check()
  }
}

const Minesweeper = class {
  static for(config) {
    return new Minesweeper(config)
  }

  constructor(config) {
    this.board = Board.with(config)

    this._status = 'ready to play'
  }

  checkPosition(id) {
    if (this._thereIsABombInPosition(id)) {
      this._status = 'LOSE'
    } else {
      this._status = 'WIN'
    }
  }

  status() {
    return this._status
  }

  _thereIsABombInPosition(id) {
    return (this.board.checkPosition(id) === 'BOOM!!!!')
  }
}

module.exports = { Minesweeper }
