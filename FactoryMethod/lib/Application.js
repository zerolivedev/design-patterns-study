const closeDialog = 'closeDialog'

const Dialog = class {
  createButton() {}

  render() {
    const okButton = this.createButton()
    okButton.onClick(closeDialog)
    return okButton.render()
  }
}

const DesktopButton = class {
  constructor() {
    this.value = 'desktop '
  }

  onClick(element) {
    this.value += `${element} clicked`
  }

  render() {
    return this.value
  }
}

const HTMLButton = class {
  constructor() {
    this.value = 'web '
  }

  onClick(element) {
    this.value += `${element} clicked`
  }

  render() {
    return this.value
  }
}

const WindowsDialog = class extends Dialog {
  createButton() {
    return new DesktopButton()
  }
}

const WebDialog = class extends Dialog {
  createButton() {
    return new HTMLButton()
  }
}

const Application = class {
  static for(config) {
    return new Application(config)
  }

  constructor(config) {
    this.dialog = new Dialog()

    if (config.os === 'desktop') {
      this.dialog = new WindowsDialog()
    } else if (config.os === 'web') {
      this.dialog = new WebDialog()
    } else {
      throw 'Invalid SO'
    }
  }

  main() {
    return this.dialog.render()
  }
}

module.exports = { Application }
