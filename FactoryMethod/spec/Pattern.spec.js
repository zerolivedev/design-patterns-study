const { expect } = require('chai')
const { Application } = require('../lib/Application')

describe('Application', () => {
  it('renders a desktop button clicked', () => {
    const systemConfig = { os: 'desktop' }
    const application = Application.for(systemConfig)

    const app = application.main()

    expect(app).to.eq('desktop closeDialog clicked')
  })

  it('renders a web button clicked', () => {
    const systemConfig = { os: 'web' }
    const application = Application.for(systemConfig)

    const app = application.main()

    expect(app).to.eq('web closeDialog clicked')
  })
})
