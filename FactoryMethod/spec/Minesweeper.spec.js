const { expect } = require('chai')
const { Minesweeper } = require('../lib/Minesweeper')

describe('Minesweeper', () => {
  it('explodes on check a bomb position', () => {
    const config = { width: 1, height: 1, bombs: 1 }
    const game = Minesweeper.for(config)

    game.checkPosition(1)

    expect(game.status()).to.eq('LOSE')
  })

  it('does nothing on check an empty position', () => {
    const config = { width: 1, height: 1, bombs: 0 }
    const game = Minesweeper.for(config)

    game.checkPosition(1)

    expect(game.status()).to.eq('WIN')
  })
})
