import { expect } from "chai"

type notificationDescriptor = {
  channels: Array<string>,
  message: string,
  videoURL:string|null
}

interface IsNotification {
  describe():notificationDescriptor
}

class Notification implements IsNotification {
  private message:string

  constructor(message:string) {
    this.message = message
  }

  describe():notificationDescriptor {
    return {
      channels: ["email"],
      message: this.message,
      videoURL: null
    }
  }
}

class DiscordNotificationDecorator implements IsNotification {
  private notification:IsNotification

  constructor(notification:IsNotification) {
    this.notification = notification
  }

  describe():notificationDescriptor {
    const descriptor:notificationDescriptor = this.notification.describe()

    descriptor.channels.push("discord")

    return descriptor
  }
}

class TikTokNotificationDecorator implements IsNotification {
  private notification:IsNotification

  constructor(notification:IsNotification) {
    this.notification = notification
  }

  describe():notificationDescriptor {
    const descriptor:notificationDescriptor = this.notification.describe()

    descriptor.channels.push("tiktok")
    descriptor.videoURL = "http://tiktok.com/myVideo"

    return descriptor
  }
}

function retrieveFrom(notification:IsNotification):notificationDescriptor {
  return  notification.describe()
}

describe("GameUpdatesNotificator", () => {
  const message:string = "New DLC available"

  it("retrieves the base email notification", () => {
    const emailNotification:IsNotification = new Notification(message)

    const descriptor:notificationDescriptor = retrieveFrom(emailNotification)

    expect(descriptor.channels).to.have.same.members(["email"])
    expect(descriptor.message).to.eq(message)
    expect(descriptor.videoURL).to.eq(null)
  })

  it("retrieves a decorated discord notification", () => {
    const emailNotification:IsNotification = new Notification(message)
    const discordNotification:IsNotification = new DiscordNotificationDecorator(emailNotification)

    const descriptor:notificationDescriptor = retrieveFrom(discordNotification)

    expect(descriptor.channels).to.have.same.members(["email", "discord"])
    expect(descriptor.message).to.eq(message)
    expect(descriptor.videoURL).to.eq(null)
  })

  it("retrieves a decorated tiktok notification", () => {
    const emailNotification:IsNotification = new Notification(message)
    const tiktokNotification:IsNotification = new TikTokNotificationDecorator(emailNotification)

    const descriptor:notificationDescriptor = retrieveFrom(tiktokNotification)

    expect(descriptor.channels).to.have.same.members(["email", "tiktok"])
    expect(descriptor.message).to.eq(message)
    expect(descriptor.videoURL).to.eq("http://tiktok.com/myVideo")
  })

  it("retrieves a redecorated discord + tiktok notification", () => {
    const emailNotification:IsNotification = new Notification(message)
    const discordNotification:IsNotification = new DiscordNotificationDecorator(emailNotification)
    const tiktokNotification:IsNotification = new TikTokNotificationDecorator(discordNotification)

    const descriptor:notificationDescriptor = retrieveFrom(tiktokNotification)

    expect(descriptor.channels).to.have.same.members(["email", "discord", "tiktok"])
    expect(descriptor.message).to.eq(message)
    expect(descriptor.videoURL).to.eq("http://tiktok.com/myVideo")
  })
})
