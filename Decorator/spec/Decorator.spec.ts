import { expect } from "chai"

interface Component {
  operation():string
}

class ConcreteComponent implements Component {
  public operation():string {
    return "ConcreteComponent"
  }
}

class ConcreteDecoratorA implements Component {
  private component:Component

  constructor(component:Component) {
    this.component = component
  }

  public operation():string {
    return `ConcreteDecoratorA(${this.component.operation()})`
  }
}

class ConcreteDecoratorB implements Component {
  private component:Component

  constructor(component:Component) {
    this.component = component
  }

  operation():string {
    return `ConcreteDecoratorB(${this.component.operation()})`
  }
}

function clientCode(component:Component):string {
  return `RESULT: ${component.operation()}`
}

describe("Decorator", () => {
  it("operates a concrete component", () => {
    const component:Component = new ConcreteComponent()

    const operation:string = clientCode(component)

    expect(operation).to.eq("RESULT: ConcreteComponent")
  })

  it("decorating the ConcreteComponent adds new behavior to the operation", () => {
    const component:Component = new ConcreteComponent()
    const decoratedComponent:Component = new ConcreteDecoratorA(component);

    const operation:string = clientCode(decoratedComponent)

    expect(operation).to.eq("RESULT: ConcreteDecoratorA(ConcreteComponent)")
  })

  it("redecorating the ConcreteComponent adds more new behavior to the operation", () => {
    const component:Component = new ConcreteComponent()
    const decoratedComponent:Component = new ConcreteDecoratorA(component);
    const redecoratedComponent:Component = new ConcreteDecoratorB(decoratedComponent);

    const operation:string = clientCode(redecoratedComponent)

    expect(operation).to.eq("RESULT: ConcreteDecoratorB(ConcreteDecoratorA(ConcreteComponent))")
  })
})
