
const Frame = class {
  constructor() {
    this.rolls = []

    this.nextFrame = null
  }

  annotate(roll) {
    if (!this._areRollsFull()) {
      this._annotateInThisFrame(roll)
    } else {
      this._annotateInNextFrame(roll)
    }
  }

  totalScore() {
    if (!this.nextFrame) {
      return this._score()
    } else {
      return this._score() + this.nextFrame.totalScore()
    }
  }

  firstRoll() {
    return this.rolls[0]
  }

  _annotateInNextFrame(roll) {
    if (!this.nextFrame) {
      this.nextFrame = new Frame()
    }

    this.nextFrame.annotate(roll)
  }

  _annotateInThisFrame(roll) {
    this.rolls.push(roll)
  }

  _score() {
    let score = this.rolls[0] + this.rolls[1]

    if (score === 10) {
      return score + this.nextFrame.firstRoll()
    }

    return score
  }

  _areRollsFull() {
    return (this.rolls.length === 2)
  }
}

const BowlingFrames = class {
  static calculatePointsFor(rolls) {
    const frame = new Frame()

    rolls.forEach((roll) => {
      frame.annotate(roll)
    });

    return frame.totalScore()
  }
}

module.exports = { BowlingFrames }
