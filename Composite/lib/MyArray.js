
const MyInteger = class {
  constructor(child) {
    this.child = child
  }
  transformsToString() {
    return this.child.toString()
  }
}

const MyArray = class {
  constructor() {
    this.children = []
  }

  push(child) {
    this.children.push(child)
  }

  transformsToString() {
    return "[" + this._childrenAsString() + "]"
  }

  _childrenAsString() {
    const childrenAsString = this.children.map((child) => {
      return child.transformsToString()
    })

    return childrenAsString.join(',')
  }
}

module.exports = { MyInteger, MyArray }
