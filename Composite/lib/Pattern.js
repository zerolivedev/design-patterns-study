
const Component = class {
  parent = null

  add(component) {
    throw "NotImplemented method"
  }

  operation() {
    throw "NotImplemented method"
  }

  isComposite() {
    return false
  }
}

const Leaf = class extends Component {
  operation() {
    return 'Leaf'
  }
}

const Composite = class extends Component {
  constructor() {
    super()

    this.children = []
  }

  add(component) {
    this.children.push(component)

    this._iAmYourFather(component)
  }

  operation() {
    const results = []

    this.children.forEach((child) => {
      results.push(child.operation())
    })

    return `Branch(${results.join('+')})`
  }

  isComposite() {
    return true
  }

  _iAmYourFather(component) {
    component.parent = this
  }
}

module.exports = { Leaf, Composite }
