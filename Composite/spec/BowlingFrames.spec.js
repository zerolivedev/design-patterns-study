const { expect } = require("chai")
const { BowlingFrames } = require("../lib/BowlingFrames")

describe("BowlingFrames", () => {
  it("calculates 0 points for no pins down in rolls", () => {
    const rolls = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

    const points = BowlingFrames.calculatePointsFor(rolls)

    expect(points).to.eq(0)
  })

  it("calculates points for ten pins down in rolls", () => {
    const rolls = [0,0,0,0,0,0,0,5,5,0,0,0,0,0,0,0,0,0,0,0]

    const points = BowlingFrames.calculatePointsFor(rolls)

    expect(points).to.eq(10)
  })

  it("calculates points for spare roll", () => {
    const rolls = [0,0,0,0,0,0,0,0,5,5,1,1,0,0,0,0,0,0,0,0]

    const points = BowlingFrames.calculatePointsFor(rolls)

    expect(points).to.eq(13)
  })
})
