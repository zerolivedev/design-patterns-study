const { expect } = require("chai")
const { MyArray, MyInteger } = require("../lib/MyArray")

describe("MyInteger", () => {
  it("transforms to string", () => {
    const integer = new MyInteger(0)

    const string = integer.transformsToString()

    expect(string).to.eq("0")
  })
})

describe("MyArray", () => {
  it("transforms to string", () => {
    const array = new MyArray()

    const string = array.transformsToString()

    expect(string).to.eq("[]")
  })

  it("transforms to string with a integer", () => {
    const zero = new MyInteger(0)
    const array = new MyArray()
    array.push(zero)

    const string = array.transformsToString()

    expect(string).to.eq("[0]")
  })

  it("transforms to string with two integer", () => {
    const zero = new MyInteger(0)
    const array = new MyArray()
    array.push(zero)
    array.push(zero)

    const string = array.transformsToString()

    expect(string).to.eq("[0,0]")
  })

  it("transforms to string with an array with values", () => {
    const anArray = new MyArray()
    const anotherArray = new MyArray()
    const zero = new MyInteger(0)
    anotherArray.push(zero)
    anotherArray.push(zero)
    anArray.push(anotherArray)
    anArray.push(zero)

    const string = anArray.transformsToString()

    expect(string).to.eq("[[0,0],0]")
  })

  it("", () => {
    const anArray = new MyArray()
    const anotherArray = new MyArray()
    anArray.push(anotherArray)

    const string = anArray.transformsToString()

    expect(string).to.eq("[[]]")
  })
})
