const { expect } = require("chai")
const { Composite, Leaf } = require("../lib/Pattern")

describe("Composite", () => {
  it("simple component", () => {
    const simple = new Leaf()

    const result = aClientCode(simple)

    expect(result).to.eq('RESULT: Leaf')
  })

  it("Now I've got a composite tree", () => {
    const tree = new Composite()
    const branch1 = new Composite()
    branch1.add(new Leaf())
    branch1.add(new Leaf())
    const branch2 = new Composite()
    branch2.add(new Leaf())
    tree.add(branch1)
    tree.add(branch2)

    const result = aClientCode(tree)

    expect(result).to.eq("RESULT: Branch(Branch(Leaf+Leaf)+Branch(Leaf))")
  })

  it("I don't need to check the components classes even when managing the tree", () => {
    const simple = new Leaf()
    const tree = new Composite()
    const branch1 = new Composite()
    branch1.add(new Leaf())
    branch1.add(new Leaf())
    const branch2 = new Composite()
    branch2.add(new Leaf())
    tree.add(branch1)
    tree.add(branch2)

    const result = anotherClientCode(tree, simple)

    expect(result).to.eq("RESULT: Branch(Branch(Leaf+Leaf)+Branch(Leaf)+Leaf)")
  })

  function aClientCode(component) {
    return `RESULT: ${component.operation()}`
  }

  function anotherClientCode(aComponent, anotherComponent) {
    if (aComponent.isComposite()) {
      aComponent.add(anotherComponent)
     }

     return `RESULT: ${aComponent.operation()}`
  }
})
