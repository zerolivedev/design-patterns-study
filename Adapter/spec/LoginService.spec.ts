import { expect } from "chai"

class Login {
  private username:string
  private password:string

  constructor(username:string, password:string) {
    this.username = username
    this.password = password
  }

  public retrieveUsername():string {
    return this.username
  }

  public retrievePassword():string {
    return this.password
  }
}

class LoginAdapter extends Login {
  public static fromExternal(externalLogin:ExternalLogin):LoginAdapter {
    return new LoginAdapter(externalLogin)
  }

  private static extractUsernameFrom(externalLogin:ExternalLogin):string {
    return externalLogin.getUsername()
  }

  private static extractPasswordFrom(externalLogin:ExternalLogin):string {
    return externalLogin.getPassword()
  }

  constructor(externalLogin:ExternalLogin) {
    const username:string = LoginAdapter.extractUsernameFrom(externalLogin)
    const password:string = LoginAdapter.extractPasswordFrom(externalLogin)

    super(username, password)
  }
}

class ExternalLogin {
  private data:string

  constructor(data:string) {
    this.data = data
  }

  public getUsername():string {
    return this.data
  }

  public getPassword():string {
    return this.data
  }
}

class LoginService {
  public static isSuccess(login:Login) {
    const hasUsername:boolean = (login.retrieveUsername() !== "")
    const hasPassword:boolean = (login.retrievePassword() !== "")

    return hasUsername && hasPassword
  }
}

describe("Login Service", () => {
  it("knows when a login is success", () => {

    const login:Login = new Login('username', 'password')

    expect(LoginService.isSuccess(login)).to.eq(true)
  })

  it("knows when a external login is success", () => {
    const externalLogin:ExternalLogin = new ExternalLogin("externalData")
    const externalUsername:string = externalLogin.getUsername()
    const externalPassword:string = externalLogin.getPassword()

    const login:Login = new Login(externalUsername, externalPassword)
    const adaptedLogin:Login = LoginAdapter.fromExternal(externalLogin)

    expect(LoginService.isSuccess(login)).to.eq(true)
    expect(LoginService.isSuccess(adaptedLogin)).to.eq(true)
  })
})
