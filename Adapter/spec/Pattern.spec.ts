import { expect } from "chai"

class Target {
  public request():string {
    return "Target: The default target's behavior."
  }
}

class Adaptee {
  public specificRequest():string {
    return ".eetpadA eht fo roivaheb laicepS"
  }
}

class Adapter extends Target {
  private adaptee:Adaptee

  constructor(adaptee:Adaptee) {
    super()

    this.adaptee = adaptee
  }

  public request(): string {
    const result:string = this.adaptee.specificRequest().split('').reverse().join('')
    return `Adapter: (TRANSLATED) ${result}`;
  }
}

function clientCode(target:Target):string {
  return target.request()
}

describe('Adapter Patter', () => {
  it('target behavior', () => {
    const target:Target = new Target()

    const adaptedRequest:string = clientCode(target)

    expect(adaptedRequest).to.eq("Target: The default target's behavior.")
  })

  it("adaptee specific behavior", () => {

    const adaptee = new Adaptee()

    expect(adaptee.specificRequest()).to.eq(".eetpadA eht fo roivaheb laicepS")
  })

  it("adapts new object to expected behavior", () => {
    const adaptee = new Adaptee()
    const adapter:Adapter = new Adapter(adaptee)

    const adaptedRequest:string = clientCode(adapter)

    expect(adaptedRequest).to.eq("Adapter: (TRANSLATED) Special behavior of the Adaptee.")
  })
})
