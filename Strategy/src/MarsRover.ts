import { MoveToNorthStrategy } from "./MarsRover/MoveToNorthStrategy"
import { MoveToSouthStrategy } from "./MarsRover/MoveToSouthStrategy"
import { MoveToEastStrategy } from "./MarsRover/MoveToEastStrategy"
import { MoveToWestStrategy } from "./MarsRover/MoveToWestStrategy"
import { NullStrategy } from "./MarsRover/NullStrategy"
import { coordinates } from "./MarsRover/coordinates"
import { Context } from "./MarsRover/Context"

export class MarsRover {
  public static landIn(facingDirection:string, coordinates:coordinates):MarsRover {
    return new MarsRover(facingDirection, coordinates)
  }

  private facingDirection:string
  private coordinates:coordinates

  constructor(facingDirection:string, coordinates:coordinates) {
    this.facingDirection = facingDirection
    this.coordinates = coordinates
  }

  public executeCommand(_command:string):void {
    const context:Context = new Context(new NullStrategy())

    if (this.isFacingNorth()) {
      context.prepareWith(new MoveToNorthStrategy())
    } else if (this.isFacingSouth()) {
      context.prepareWith(new MoveToSouthStrategy())
    } else if (this.isFacingEast()) {
      context.prepareWith(new MoveToEastStrategy())
    } else if (this.isFacingWest()) {
      context.prepareWith(new MoveToWestStrategy())
    }

    this.coordinates = context.calculateDestinationFor(this.coordinates)
  }

  public currentPosition():coordinates {
    return this.coordinates
  }

  private isFacingNorth():boolean {
    return (this.facingDirection === "north")
  }

  private isFacingSouth():boolean {
    return (this.facingDirection === "south")
  }

  private isFacingEast():boolean {
    return (this.facingDirection === "east")
  }

  private isFacingWest():boolean {
    return (this.facingDirection === "west")
  }
}
