import { coordinates } from "./coordinates"
import { Strategy } from "./Strategy"

export class MoveToWestStrategy implements Strategy {
  calculateDestinationFor(coordinates:coordinates):coordinates {
    const newCoordinates:coordinates = {...coordinates}

    newCoordinates.latitude -= 1

    return newCoordinates
  }
}
