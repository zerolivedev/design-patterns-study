import { coordinates } from "./coordinates";
import { Strategy } from "./Strategy";

export class NullStrategy implements Strategy {
  public calculateDestinationFor(coordinates:coordinates):coordinates {
    return coordinates
  }
}
