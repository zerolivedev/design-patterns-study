import { coordinates } from "./coordinates"
import { Strategy } from "./Strategy"

export class MoveToSouthStrategy implements Strategy {
  calculateDestinationFor(coordinates:coordinates):coordinates {
    const newCoordinates:coordinates = {...coordinates}

    newCoordinates.longitude -= 1

    return newCoordinates
  }
}
