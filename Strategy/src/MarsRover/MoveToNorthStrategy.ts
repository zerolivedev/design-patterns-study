import { coordinates } from "./coordinates"
import { Strategy } from "./Strategy"

export class MoveToNorthStrategy implements Strategy {
  calculateDestinationFor(coordinates:coordinates):coordinates {
    const newCoordinates:coordinates = {...coordinates}

    newCoordinates.longitude += 1

    return newCoordinates
  }
}
