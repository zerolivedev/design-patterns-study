import { coordinates } from "./coordinates"

export interface Strategy {
  calculateDestinationFor(coordinates:coordinates):coordinates
}
