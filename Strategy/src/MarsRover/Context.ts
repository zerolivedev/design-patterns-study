import { coordinates } from "./coordinates";
import { Strategy } from "./Strategy";


export class Context {
  private strategy:Strategy

  constructor(strategy:Strategy) {
    this.strategy = strategy
  }

  public prepareWith(strategy:Strategy):void {
    this.strategy = strategy
  }

  public calculateDestinationFor(coordinates:coordinates):coordinates {
    return this.strategy.calculateDestinationFor(coordinates)
  }
}
