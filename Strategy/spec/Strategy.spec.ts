import { coordinates } from "../src/MarsRover/coordinates"
import { MarsRover } from "../src/MarsRover"
import { expect } from "chai"

describe("Mars Rover", () => {
  describe("on move forward", () => {
    it("facing North increases the longitude", () => {
      const coordinates:coordinates = { longitude: 1, latitude: 0 }
      const facingNorth:string = "north"
      const rover:MarsRover = MarsRover.landIn(facingNorth, coordinates)

      rover.executeCommand("M")

      expect(rover.currentPosition().longitude).to.eq(2)
      expect(rover.currentPosition().latitude).to.eq(0)
    })

    it("facing South decreases the longitude", () => {
      const coordinates:coordinates = { longitude: 0, latitude: 0 }
      const facingNorth:string = "south"
      const rover:MarsRover = MarsRover.landIn(facingNorth, coordinates)

      rover.executeCommand("M")

      expect(rover.currentPosition().longitude).to.eq(-1)
      expect(rover.currentPosition().latitude).to.eq(0)
    })

    it("facing East increases the latitude", () => {
      const coordinates:coordinates = { longitude: 0, latitude: 0 }
      const facingNorth:string = "east"
      const rover:MarsRover = MarsRover.landIn(facingNorth, coordinates)

      rover.executeCommand("M")

      expect(rover.currentPosition().longitude).to.eq(0)
      expect(rover.currentPosition().latitude).to.eq(1)
    })

    it("facing West decreases the latitude", () => {
      const coordinates:coordinates = { longitude: 0, latitude: 0 }
      const facingNorth:string = "west"
      const rover:MarsRover = MarsRover.landIn(facingNorth, coordinates)

      rover.executeCommand("M")

      expect(rover.currentPosition().longitude).to.eq(0)
      expect(rover.currentPosition().latitude).to.eq(-1)
    })
  })
})
