const { expect } = require("chai")
const { DBConnection } = require("../lib/DBConnection")

describe("DBConnection", () => {
  it("retrieves a connection", () => {

    const connection = DBConnection.connect()

    expect(connection).not.to.eq(undefined)
  })

  it("retrieves the same connection once is connected", () => {

    const aConnection = DBConnection.connect()
    const anotherConnection = DBConnection.connect()

    expect(aConnection).to.eq(anotherConnection)
  })

  it("retrieves a new connection after be disconnected", () => {
    const aConnection = DBConnection.connect()
    aConnection.disconnect()

    const anotherConnection = DBConnection.connect()

    expect(aConnection).not.to.eq(anotherConnection)
  })
})
