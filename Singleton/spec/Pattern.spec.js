const { expect } = require("chai")
const { Singleton } = require("../lib/Pattern")

describe("Singleton", () => {
  beforeEach(() => {
    resetInstancesOfSingleton()
  })

  it("retrieves a instance of the object", () => {

    const singleton = Singleton.retrieveInstance()

    expect(singleton.id).not.to.eq(undefined)
  })

  it("retrieves a new instance every time is reset", () => {
    const firstSingleton = Singleton.retrieveInstance()
    resetInstancesOfSingleton()

    const secondSingleton = Singleton.retrieveInstance()

    expect(firstSingleton.id).not.to.eq(secondSingleton.id)
  })

  it("retrieves always the same instance", () => {

    const firstSingleton = Singleton.retrieveInstance()
    const secondSingleton = Singleton.retrieveInstance()

    expect(firstSingleton.id).to.eq(secondSingleton.id)
  })

  function resetInstancesOfSingleton() {
    Singleton._instance = null
  }
})
