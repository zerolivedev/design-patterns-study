const { uniqueID } = require("./uniqueID")

const Singleton = class {
  static retrieveInstance() {
    if (this._instance === null) {
      this._instance = new Singleton()
    }

    return this._instance
  }

  static _instance = null

  // Private
  constructor() {
    this.id = uniqueID()
  }
}

module.exports = { Singleton }
