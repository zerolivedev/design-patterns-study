const DBConnection = class {
  static connect() {
    if(this._instance === null) {
      this._instance = new DBConnection()
    }

    return this._instance
  }

  static _instance = null

  disconnect() {
    DBConnection._instance = null
  }
}

module.exports = { DBConnection }
