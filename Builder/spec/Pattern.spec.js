const { expect } = require("chai")

const Product = class {
  constructor() {
    this.parts = []
  }

  addPart(name) {
    this.parts.push('Part' + name)
  }

  listParts() {
    const parts = this.parts.join(', ')

    return "Product parts: " + parts
  }
}

const ConcreteBuilder1 = class {
  constructor() {
    this.product = new Product()
  }

  addPartA() {
    this.product.addPart("A1")
  }

  addPartB() {
    this.product.addPart("B1")
  }

  addPartC() {
    this.product.addPart("C1")
  }

  getProduct() {
    return this.product
  }
}

const ConcreteBuilder2 = class {
  constructor() {
    this.product = new Product()
  }

  addPartA() {
    this.product.addPart("A2")
  }

  addPartB() {
    this.product.addPart("B2")
  }

  addPartC() {
    this.product.addPart("C2")
  }

  getProduct() {
    return this.product
  }
}

const Director = class {
  setBuilder(builder) {
    this.builder = builder
  }

  buildMinimalViableProduct() {
    this.builder.addPartA()
  }

  buildFullFeaturedProduct() {
    this.builder.addPartA()
    this.builder.addPartB()
    this.builder.addPartC()
  }
}

describe("Builder", () => {
  let director
  let builder

  beforeEach(() => {
    director = new Director()
    builder = new ConcreteBuilder1()
    director.setBuilder(builder)
  })

  it("builds Standard basic product", () => {

    director.buildMinimalViableProduct()

    const parts = builder.getProduct().listParts()
    expect(parts).to.eq('Product parts: PartA1')
  })

  it("builds Standard full featured product", () => {

    director.buildFullFeaturedProduct();

    const parts = builder.getProduct().listParts();
    expect(parts).to.eq('Product parts: PartA1, PartB1, PartC1')
  })

  it("builds custom products", () => {

    builder.addPartA()
    builder.addPartC()

    const parts = builder.getProduct().listParts()
    expect(parts).to.eq('Product parts: PartA1, PartC1')
  })

  it("builds another Standard basic product", () => {
    builder = new ConcreteBuilder2()
    director.setBuilder(builder)

    director.buildMinimalViableProduct()

    const parts = builder.getProduct().listParts()
    expect(parts).to.eq('Product parts: PartA2')
  })

  it("builds another Standard full featured product", () => {
    builder = new ConcreteBuilder2()
    director.setBuilder(builder)

    director.buildFullFeaturedProduct();

    const parts = builder.getProduct().listParts();
    expect(parts).to.eq('Product parts: PartA2, PartB2, PartC2')
  })
})
