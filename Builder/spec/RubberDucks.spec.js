const { expect } = require("chai")

const StrongRubberDuck = class {
  constructor(descriptor) {
    this.color = descriptor.color
    this.accessories = descriptor.accessories
    this.costume = descriptor.costume
  }

  description() {
    return "Strong " + this.color + " " + this.accessories + " " + this.costume
  }
}

const StrongRubberDucksBuilder = class {
  constructor() {
    this.descriptor = {
      color: "Yellow",
      accessories: "without accessories",
      costume: "without costume"
    }
  }

  build() {
    return new StrongRubberDuck(this.descriptor)
  }
}

const RubberDuck = class {
  constructor(descriptor) {
    this.color = descriptor.color
    this.accessories = descriptor.accessories
    this.costume = descriptor.costume
  }

  description() {
    return this.color + " " + this.accessories + " " + this.costume
  }
}

const RubberDucksBuilder = class {
  constructor() {
    this.descriptor = {
      color: "Yellow",
      accessories: "without accessories",
      costume: "without costume"
    }
  }

  aBlueDuck() {
    this.descriptor.color = "Blue"

    return this
  }

  aPinkDuck() {
    this.descriptor.color = "Pink"

    return this
  }

  aGreenDuck() {
    this.descriptor.color = "Green"

    return this
  }

  with() {
    return this
  }

  sharkCostume() {
    this.descriptor.costume = "with shark costume"

    return this
  }

  aFiremanCostume() {
    this.descriptor.costume = "with fireman costume"

    return this
  }

  aHat() {
    this.descriptor.accessories = "with a hat"

    return this
  }

  aHelmet() {
    this.descriptor.accessories = "with a helmet"

    return this
  }

  build() {
    return new RubberDuck(this.descriptor)
  }
}

const Director = class {
  use(builder) {
    this.builder = builder
  }

  buildBasicRobberDuck() {
    return this.builder.build()
  }
}

describe("RubberDucksBuilder", () => {
  it("builds koduck", () => {
    const builder = new RubberDucksBuilder()

    const rubberDuck = builder.build()

    expect(rubberDuck.description()).to.eq("Yellow without accessories without costume")
  })

  it("builds a green duck with shark costume", () => {
    const builder = new RubberDucksBuilder()

    const rubberDuck = builder.aGreenDuck().with().sharkCostume().build()

    expect(rubberDuck.description()).to.eq("Green without accessories with shark costume")
  })

  it("builds a pink duck with a hat", () => {
    const builder = new RubberDucksBuilder()

    const rubberDuck = builder.aPinkDuck().with().aHat().build()

    expect(rubberDuck.description()).to.eq("Pink with a hat without costume")
  })

  it("builds a blue duck with a helmet with fireman costume", () => {
    const builder = new RubberDucksBuilder()

    const rubberDuck = builder.aBlueDuck().with().aHelmet().with().aFiremanCostume().build()

    expect(rubberDuck.description()).to.eq("Blue with a helmet with fireman costume")
  })
})

describe("Director", () => {
  it("builds normal rubber ducks", () => {
    const director = new Director()
    const builder = new RubberDucksBuilder()
    director.use(builder)

    const rubberDuck = director.buildBasicRobberDuck()

    expect(rubberDuck.description()).to.eq("Yellow without accessories without costume")
  })

  it("builds strong rubber ducks", () => {
    const director = new Director()
    const builder = new StrongRubberDucksBuilder()
    director.use(builder)

    const rubberDuck = director.buildBasicRobberDuck()

    expect(rubberDuck.description()).to.eq("Strong Yellow without accessories without costume")
  })
})
