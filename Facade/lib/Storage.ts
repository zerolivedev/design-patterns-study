class Cloud {
  public upload(_filename:string):void {}
}

class SFTP {
  public upload(_filename:string):void {}
}

export type Registry = { filename:string, service:string }

class DB {
  private registries:Array<Registry> = []

  public register(filename:string, service: string): void {
    this.registries.push({ filename, service })
  }

  public retrieveRegistryFor(_filename:string):Registry {
    return this.registries[0]
  }
}

export class Storage {
  private cloud:Cloud
  private sftp:SFTP
  private db:DB

  public static configured():Storage {
    const cloud:Cloud = new Cloud()
    const sftp:SFTP = new SFTP()
    const db:DB = new DB()

    const storage:Storage = new Storage(cloud, sftp, db)

    return storage
  }

  private constructor(cloud:Cloud, sftp:SFTP, db:DB) {
    this.cloud = cloud
    this.sftp = sftp
    this.db = db
  }

  public store(filename:string):Registry {
    if (filename.includes('huge')) {
      this.cloud.upload(filename)
      this.db.register(filename, 'cloud')
    }

    if (filename.includes('light')) {
      this.sftp.upload(filename)
      this.db.register(filename, 'sftp')
    }

    return this.db.retrieveRegistryFor(filename)
  }
}
