import { expect } from "chai"
import { Subsystem1, Subsystem2, Facade } from "../lib/Pattern"

describe('Facade', () => {
  it('operates subsystems', () => {
    const subsystem1:Subsystem1 = new Subsystem1()
    const subsystem2:Subsystem2 = new Subsystem2()
    const facade:Facade = new Facade(subsystem1, subsystem2)

    const result:string = facade.operation()

    expect(result).to.eq("Facade initializes subsystems:\nSubsystem1: Ready!\nSubsystem2: Get ready!\nFacade orders subsystems to perform the action:\nSubsystem1: Go!\nSubsystem2: Fire!")
  })
})
