import { expect } from "chai"
import { Registry, Storage } from "../lib/Storage"

describe('Storage', () => {
  it('registers that huge files are stored in cloud service', () => {
    const storage:Storage = Storage.configured()
    const hugeFilename:string = 'hugeFile.mkv'

    const registry:Registry = storage.store(hugeFilename)

    expect(registry.filename).to.eq(hugeFilename)
    expect(registry.service).to.eq('cloud')
  })

  it('registers that light files are stored in sftp service', () => {
    const storage:Storage = Storage.configured()
    const lightFilename:string = 'lightTextFile.txt'

    const registry:Registry = storage.store(lightFilename)

    expect(registry.filename).to.eq(lightFilename)
    expect(registry.service).to.eq('sftp')
  })
})
