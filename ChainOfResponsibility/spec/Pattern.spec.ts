import { expect } from "chai"

type OrderRequest = {
  user:string,
  data:Array<string>
}

type OrderResponse = {
  status:string,
  data:Array<string>
}

interface Handler {
  setNext(handler:Handler):void
  handle(request:OrderRequest):OrderResponse
}

class NoHandler implements Handler {
  setNext(_handler:Handler):void {}
  handle(request:OrderRequest):OrderResponse {
    return {
      status: 'ordered',
      data: request.data
    }
  }
}

class SanitizerHandler implements Handler {
  private nextHandler?:Handler

  setNext(handler:Handler):void {
    this.nextHandler = handler
  }

  handle(request:OrderRequest):OrderResponse {
    const sanitizedData:Array<string> = request.data.map(item => item.toLowerCase())

    if(this.nextHandler) {
      return this.nextHandler.handle({
        user: request.user,
        data: sanitizedData,
      })
    } else {
      return {
        status: 'ordered',
        data: sanitizedData
      }
    }
  }
}

class BlackfridayHandler implements Handler {
  private nextHandler?:Handler

  setNext(handler:Handler):void {
    this.nextHandler = handler
  }

  handle(request:OrderRequest):OrderResponse {
    const newData:Array<string> = [...request.data]
    newData.push('gift')

    if (this.nextHandler) {
      return this.nextHandler.handle({
        user: request.user,
        data: newData
      })
    } else {
      return {
        status: "ordered",
        data: newData
      }
    }
  }
}

class Application {
  private handler:Handler = new NoHandler()

  addHandler(handler:Handler) {
    this.handler = handler
  }

  process(request:OrderRequest):OrderResponse {
    const response = this.handler.handle(request)

    return response
  }
}

describe("Chain Of Responsibility", () => {
  it("process a request without handlers", () => {
    const data:Array<string> = ["a book", "another book"]
    const request:OrderRequest = { user: "user@email.com", data }
    const application:Application = new Application()

    const response:OrderResponse = application.process(request)

    expect(response.status).to.eq("ordered")
  })

  it("process a request remove unnecessary data", () => {
    const request:OrderRequest = { user: "user@email.com", data: ["A book", "anOther boOK"] }
    const sanitizerHandler:Handler = new SanitizerHandler()
    const application:Application = new Application()
    application.addHandler(sanitizerHandler)

    const response:OrderResponse = application.process(request)

    expect(response.status).to.eq("ordered")
    expect(response.data).to.include.members(["a book", "another book"])
  })

  it("process a request from Blackfriday", () => {
    const request:OrderRequest = { user: "user@email.com", data: ["a book", "another book"] }
    const blackfridayHandler:Handler = new BlackfridayHandler()
    const application:Application = new Application()
    application.addHandler(blackfridayHandler)

    const response:OrderResponse = application.process(request)

    expect(response.status).to.eq("ordered")
    expect(response.data).to.include.members(["a book", "another book", "gift"])
  })

  it("process a request with multiple handlers", () => {
    const request:OrderRequest = { user: "user@email.com", data: ["A book", "anOther boOK"] }
    const blackfridayHandler:Handler = new BlackfridayHandler()
    const sanitizerHandler:Handler = new SanitizerHandler()
    blackfridayHandler.setNext(sanitizerHandler)
    const application:Application = new Application()
    application.addHandler(blackfridayHandler)

    const response:OrderResponse = application.process(request)

    expect(response.status).to.eq("ordered")
    expect(response.data).to.include.members(["a book", "another book", "gift"])
  })

  it("process a request with multiple handlers", () => {
    const request:OrderRequest = { user: "user@email.com", data: ["A book", "anOther boOK"] }
    const sanitizerHandler:Handler = new SanitizerHandler()
    const blackfridayHandler:Handler = new BlackfridayHandler()
    sanitizerHandler.setNext(blackfridayHandler)
    const application:Application = new Application()
    application.addHandler(sanitizerHandler)

    const response:OrderResponse = application.process(request)

    expect(response.status).to.eq("ordered")
    expect(response.data).to.include.members(["a book", "another book", "gift"])
  })
})
