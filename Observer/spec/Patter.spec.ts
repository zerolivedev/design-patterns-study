import { expect } from "chai"

class ConcreteSubject {
  private value:number = 0
  private observers:Array<AObserver> = []

  state():number {
    return this.value
  }

  changeState():void {
    this.value += 1

    this.notifyToObservers()
  }

  subscribe(observer:AObserver):void {
    this.observers.push(observer)
  }

  private notifyToObservers():void {
    for(const observer of this.observers) {
      observer.update(this.value)
    }
  }
}

class AObserver {
  private value:number = -1

  update(newValue:number):void {
    this.value = newValue
  }

  state():number {
    return this.value
  }
}

describe("Observer", () => {
  it("notifies from subject to subscribed observers when its state changes", () => {
    const subject:ConcreteSubject = new ConcreteSubject()
    const observer:AObserver = new AObserver()
    subject.subscribe(observer)
    expect(subject.state()).not.to.eq(observer.state())

    subject.changeState()

    expect(subject.state()).to.eq(observer.state())
  })

  describe("ConcreteSubject ", () => {
    it("changes its state", () => {
      const subject:ConcreteSubject = new ConcreteSubject()
      const previousState:number = subject.state()

      subject.changeState()

      expect(subject.state()).not.to.eq(previousState)
    })
  })

  describe("AObserver", () => {
    it("stores a new state when its updated", () => {
      const observer:AObserver = new AObserver()
      const originalState:number = observer.state()
      const newValue:number = 19

      observer.update(newValue)

      expect(observer.state()).not.to.eq(originalState)
      expect(observer.state()).to.eq(newValue)
    })
  })
})
