import { expect } from "chai"

type subscriptions = {
  [key: string]: Array<Function>
}

class EventBus {
  private subscriptions:subscriptions = {}
  subscribe(event:string, callback:Function):void {
    if (!this.subscriptions[event]) {
      this.subscriptions[event] = []
    }

    this.subscriptions[event].push(callback)
  }

  publish(event:string, value:string) {
    this.subscriptions[event].forEach(callback => {
      callback(value)
    })
  }
}

describe("Event Bus", () => {
  it("can publish a event with a value", () => {
    let valueToUpdate:string = 'valueToUpdate'
    const newValue:string = 'newValue'
    const callback = (value:string) => { valueToUpdate = value }
    const bus:EventBus = new EventBus()
    bus.subscribe('theEvent', callback)

    bus.publish('theEvent', newValue)

    expect(valueToUpdate).to.eq(newValue)
  })
})
