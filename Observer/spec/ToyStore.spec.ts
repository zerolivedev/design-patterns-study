import { expect } from "chai"

class ToyStore {
  private toys:Array<string> = []
  private buyers:Array<Client> = []

  retrieve(toy:string):void {
    this.toys.push(toy)

    this.sendToysToBuyers()
  }

  buyOnline(buyer:Client):void {
    this.buyers.push(buyer)
  }

  stock():Array<string> {
    return this.toys
  }

  private sendToysToBuyers():void {
    this.buyers.forEach(buyer => {
      const toy = this.toys[0]

      buyer.retrieve(toy)
    })
  }
}

class Client {
  private items:Array<string> = []

  retrieve(toy:string):void {
    this.items.push(toy)
  }

  toys():Array<string> {
    return this.items
  }
}

describe("ToyStore", () => {
  const newToy:string = "newest amazing toys"

  it("retrieves a lot of newest amazing toys", () => {
    const store:ToyStore = new ToyStore()
    expect(store.stock().length).to.eq(0)

    store.retrieve(newToy)

    expect(store.stock().length).to.eq(1)
    expect(store.stock()[0]).to.eq(newToy)
  })

  it("sends the new toy to the buyers", () => {
    const store:ToyStore = new ToyStore()
    const buyer:Client = new Client()
    expect(buyer.toys().length).to.eq(0)
    store.buyOnline(buyer)

    store.retrieve(newToy)

    expect(buyer.toys().length).to.eq(1)
    expect(buyer.toys()[0]).to.eq(newToy)
  })
})
