import { expect } from "chai"

interface Service {
  call(url:string):string
}

class HttpService implements Service {
  call(url:string):string {
    return "HTTP CALL TO " + url
  }
}

class Proxy implements Service {
  private RUBBER_DUCK_CREATION_TIMESTAMP:string = "[1931/12/01 20:30]"
  private service:Service

  constructor(service:Service) {
    this.service = service
  }

  call(url:string):string {
    const response:string = this.service.call(url)

    return this.RUBBER_DUCK_CREATION_TIMESTAMP + " " + response
  }
}

class Client {
  private static URL:string = "www.zerolive.dev"
  static callWith(service:Service):string {

    return service.call(this.URL)
  }
}

describe("Proxy pattern", () => {
  const url:string = 'www.zerolive.dev'

  describe("has a Client", () => {
    it("does a specific call with a service", () => {
      const specificService:Service = new HttpService()

      const response:string = Client.callWith(specificService)

      expect(response).to.eq("HTTP CALL TO " + url)
    })

    it("does a specific call with a service in a proxy", () => {
      const httpService:Service = new HttpService()
      const proxy:Service = new Proxy(httpService)

      const response:string = Client.callWith(proxy)

      expect(response).to.eq("[1931/12/01 20:30] HTTP CALL TO " + url)
    })
  })

  describe("has a HttpService", () => {
    it("does a HTTP call", () => {
      const service:Service = new HttpService()

      const response:string = service.call(url)

      expect(response).to.eq("HTTP CALL TO " + url)
    })
  })

  describe("has a HttpProxy", () => {
    it("adds a timestamp to a service call", () => {
      const httpService:Service = new HttpService()
      const proxy:Service = new Proxy(httpService)

      const response:string = proxy.call(url)

      expect(response).to.eq("[1931/12/01 20:30] HTTP CALL TO " + url)
    })
  })
})
